#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/../hack/ensure-curl.sh
source $(dirname $0)/common.sh

if [ ! -z "${GITLAB_READ_ACCESS_TOKEN}" ]; then
  params="private_token=${GITLAB_READ_ACCESS_TOKEN}"
  
  deps_step_1_path=$(dirname $0)/../repositories/deps-step-1.bzl

  # core
  core_project_id="20397766"
  current_sha256=$(bazel query //external:fastplatform_core_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${core_project_id}/jobs/[0-9][0-9]*/.*[^\"],${core_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${core_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path

  # addons/fertilicalc
  fertilicalc_project_id="20375677"
  current_sha256=$(bazel query //external:fastplatform_addons_fertilicalc_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${fertilicalc_project_id}/jobs/[0-9][0-9]*/.*[^\"],${fertilicalc_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${fertilicalc_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path

  # addons/siar-aemet
  siar_aemet_project_id="20379107"
  current_sha256=$(bazel query //external:fastplatform_addons_siar_aemet_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${siar_aemet_project_id}/jobs/[0-9][0-9]*/.*[^\"],${siar_aemet_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${siar_aemet_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path

  # mobile/farmer-mobile-app
  farmer_mobile_app_project_id="16049031"
  current_sha256=$(bazel query //external:fastplatform_farmer_mobile_app_artifacts --output build 2>/dev/null | \
    grep sha256 | \
    tr -d '[[:space:]],' | \
    sed -e 's/sha256="\(.*\)"/\1/g')
  sed -i.bak \
    "s,${farmer_mobile_app_project_id}/jobs/[0-9][0-9]*/.*[^\"],${farmer_mobile_app_project_id}/jobs/artifacts/master/download?job=publish%3Aall,g" \
    $deps_step_1_path
  new_sha256=$(curl -L --silent \
    "$(cat $deps_step_1_path | grep -Eo https://.*/${farmer_mobile_app_project_id}/[^\"]*)&${params}" | \
    shasum -a 256 -b | cut -d' ' -f 1)
  sed -i.bak "s,${current_sha256},${new_sha256},g" $deps_step_1_path
fi