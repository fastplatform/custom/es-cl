#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/../hack/ensure-kubectl.sh
source $(dirname $0)/common.sh

if [ ! -z "${CI_ENVIRONMENT_INITIALIZATION_SCRIPT}" ]; then
  source $CI_ENVIRONMENT_INITIALIZATION_SCRIPT
fi


## Generate Kubernetes manifests

# Fetch binaries
bazel build //tools:jq //tools:kustomize
jq=$(bazel info bazel-genfiles)/tools/jq
kustomize=$(bazel info bazel-genfiles)/tools/kustomize

# Clean workspace
rm -rf $(dirname $0)/.build
mkdir -p $(dirname $0)/.build/deploy/resources

# Extract & Configure the generic patch to meet the requirements of the target environment
tar xvzf $(dirname $0)/generic-patch.tar.gz -C $(dirname $0)/.build/deploy/resources
$(dirname $0)/.build/deploy/resources/apply.sh \
  $CI_REGISTRY \
  $CI_REGISTRY_USER \
  $CI_REGISTRY_PASSWORD \
  $jq

# Build & Extract custom/ee artitfacts
bazel build //:release_kustomize_manifest.tar.gz
tar xvzf $(bazel info bazel-genfiles)/release_kustomize_manifest.tar.gz --strip-components=1  -C $(dirname $0)/.build/deploy/resources/release

# Decrypt encrypted files
echo ${CI_GIT_CRYPT_KEY} | base64 --decode > .git-crypt-key
(
cat <<EOF
core/manifests/resources/services/mobile/push_notification/keys/AuthKey_Dev_D8WQ2BZ2F6.p8
core/manifests/resources/services/mobile/push_notification/keys/api_key.txt
addons/fertilicalc/init/000.auth_user.csv
EOF
) | xargs -n 1 sh -c ' \
  export FILE_PATH='$(dirname $0)'/.build/deploy/resources/release/$0; \
  chmod +w $FILE_PATH; cat $FILE_PATH | git-crypt smudge --key-file .git-crypt-key 2>/dev/null >$FILE_PATH.tmp; \
  mv $FILE_PATH.tmp $FILE_PATH'
rm .git-crypt-key

# Final settings
cp -r ${CI_ENVIRONMENT_DIR_SETTINGS}/* $(dirname $0)/.build/deploy
(
cd $(dirname $0)/.build && \
    $kustomize create && \
    $kustomize edit add resource deploy
    $kustomize edit add resource deploy/ext-ns
)
(
  cd $(dirname $0)/.build/deploy && \
    $kustomize edit add resource resources && \
    $kustomize edit set namespace $NAMESPACE && \
    sed -i.bak \
      "s/jaeger-service-name=\(.*\)/jaeger-service-name=\1-${CI_ENVIRONMENT_SLUG}/g" confs/{*.conf,**/*.conf} && \
    echo >> confs/core.conf && \
    sed -i.bak \
      "s,web.backend.django-storage-static-s3-location=static/\([^/]*\)/.*,web.backend.django-storage-static-s3-location=static/\1/core/web-backend/${CI_ENVIRONMENT_SHORT_SLUG},g" \
      confs/core.conf && \
    sed -i.bak \
      "s,fertilization.fertilicalc.django-storage-s3-location=static/\([^/]*\)/.*,fertilization.fertilicalc.django-storage-s3-location=static/\1/addons/fertilicalc/${CI_ENVIRONMENT_SHORT_SLUG},g" \
      confs/addons/fertilicalc.conf && \
    echo "fastplatform.ci.environment-slug=${CI_ENVIRONMENT_SLUG}" >> confs/core.conf && \
    echo "fastplatform.ci.project-path-slug=${CI_PROJECT_PATH_SLUG}" >> confs/core.conf && \
    echo "fastplatform.global.domain=${CI_ENVIRONMENT_DOMAIN}" >> confs/core.conf && \
    echo "fastplatform.global.basedomain=${CI_BASE_DOMAIN}" >> confs/core.conf && \
    echo "fastplatform.global.subdomain=${CI_ENVIRONMENT_DOMAIN%.$CI_BASE_DOMAIN}" >> confs/core.conf && \
    echo "fastplatform.global.web-app-url=https://app.${CI_BASE_DOMAIN}" >> confs/core.conf && \
cat << EOF >> kustomization.yaml
vars:
- name: fastplatform.ci.environment-slug
  objref:
    kind: ConfigMap
    name: fastplatform-core-config
    apiVersion: v1
  fieldref:
    fieldpath: data['fastplatform.ci.environment-slug']
- name: fastplatform.ci.project-path-slug
  objref:
    kind: ConfigMap
    name: fastplatform-core-config
    apiVersion: v1
  fieldref:
    fieldpath: data['fastplatform.ci.project-path-slug']
- name: fastplatform.global.basedomain
  objref:
    kind: ConfigMap
    name: fastplatform-core-config
    apiVersion: v1
  fieldref:
    fieldpath: data['fastplatform.global.basedomain']
- name: fastplatform.global.subdomain
  objref:
    kind: ConfigMap
    name: fastplatform-core-config
    apiVersion: v1
  fieldref:
    fieldpath: data['fastplatform.global.subdomain']
EOF
)

# Finally produce a ready-to-deploy Kubernetes manifest
bazel run //tools:kustomize build $(pwd)/$(dirname $0)/.build > deployed-manifest.yaml


## Deployment

KUBECTL_SELECTOR_DEFAULT_ARGS="fastplatform.eu/kustomize-config!=true" # Don't apply Kustomize resources
PGO_NAMESPACE=$(kubectl get ns $NAMESPACE -o=jsonpath='{.metadata.labels.pgo-installation-name}')

# namespace
kubectl apply -f deployed-manifest.yaml --selector deploy.fastplatform.eu/k8s-resource=namespace
kubectl apply -f deployed-manifest.yaml --selector $KUBECTL_SELECTOR_DEFAULT_ARGS,!fastplatform.eu/module

# addons/fertilicalc
kubectl apply -f deployed-manifest.yaml --selector $KUBECTL_SELECTOR_DEFAULT_ARGS,fastplatform.eu/module=fertilicalc,!deploy.fastplatform.eu/db-requirement
echo -n "[+] waiting addons/fertilicalc pg instance to be ready ... "
wait_pods $NAMESPACE "name=fertilization-fertilicalc-postgres,pg-cluster=fertilization-fertilicalc-postgres"
wait_pods $NAMESPACE "name=fertilization-fertilicalc-postgres-pgbouncer,pg-cluster=fertilization-fertilicalc-postgres"
echo OK
check_pgcluster_replicas $NAMESPACE $PGO_NAMESPACE fertilization-fertilicalc-postgres
kubectl apply -f deployed-manifest.yaml --selector fastplatform.eu/module=fertilicalc,deploy.fastplatform.eu/db-requirement=started
force_knative_services_update $NAMESPACE --selector fastplatform.eu/module=fertilicalc,deploy.fastplatform.eu/db-requirement=started
if [ ! -z "${CI_ENVIRONMENT_INITIALIZATION_SCRIPT}" ]; then
  echo -n "[+] initializing addons/fertilicalc service ... "
  init_services $NAMESPACE "fertilicalc"
  echo OK
fi
echo -n "[+] running Django collectstatic on addons/fertilicalc service ... "
knative_service_exec $namespace "fertilization-fertilicalc" python ../../*.binary collectstatic
echo OK
kubectl apply -f deployed-manifest.yaml --selector fastplatform.eu/module=fertilicalc,deploy.fastplatform.eu/db-requirement=initialized
force_knative_services_update $NAMESPACE --selector fastplatform.eu/module=fertilicalc,deploy.fastplatform.eu/db-requirement=initialized

# addons/siar-aemet
kubectl apply -f deployed-manifest.yaml --selector $KUBECTL_SELECTOR_DEFAULT_ARGS,fastplatform.eu/module=siar-aemet,!deploy.fastplatform.eu/db-requirement
echo -n "[+] waiting addons/siar-aemet redis instance to be ready ... "
wait_pods $NAMESPACE "redis-cluster=meteorology-weather-redis"
echo OK
kubectl apply -f deployed-manifest.yaml --selector fastplatform.eu/module=siar-aemet,deploy.fastplatform.eu/db-requirement=started
force_knative_services_update $NAMESPACE --selector fastplatform.eu/module=siar-aemet,deploy.fastplatform.eu/db-requirement=started

# addons/sobloo
kubectl apply -f deployed-manifest.yaml --selector $KUBECTL_SELECTOR_DEFAULT_ARGS,fastplatform.eu/module=sobloo,!deploy.fastplatform.eu/db-requirement
echo -n "[+] waiting addons/sobloo pg instance to be ready ... "
wait_pods $NAMESPACE "name=tile-map-catalogue-postgres,pg-cluster=tile-map-catalogue-postgres"
wait_pods $NAMESPACE "name=tile-map-catalogue-postgres-pgbouncer,pg-cluster=tile-map-catalogue-postgres"
echo OK
check_pgcluster_replicas $NAMESPACE $PGO_NAMESPACE tile-map-catalogue-postgres
kubectl apply -f deployed-manifest.yaml --selector fastplatform.eu/module=sobloo,deploy.fastplatform.eu/db-requirement=started
force_knative_services_update $NAMESPACE --selector fastplatform.eu/module=sobloo,deploy.fastplatform.eu/db-requirement=started

# core + es-cl
create_or_replace -f deployed-manifest.yaml --selector deploy.fastplatform.eu/exceeds-character-limit=true
kubectl apply -f deployed-manifest.yaml --selector $KUBECTL_SELECTOR_DEFAULT_ARGS,'fastplatform.eu/module in (core,es-cl),!deploy.fastplatform.eu/db-requirement,!deploy.fastplatform.eu/exceeds-character-limit'
echo -n "[+] waiting core pg instances to be ready ... "
wait_pods $NAMESPACE "name=external,pg-cluster=external"
wait_pods $NAMESPACE "name=external-pgbouncer,pg-cluster=external"
wait_pods $NAMESPACE "name=fastplatform,pg-cluster=fastplatform"
wait_pods $NAMESPACE "name=fastplatform-pgbouncer,pg-cluster=fastplatform"
echo OK
check_pgcluster_replicas $NAMESPACE $PGO_NAMESPACE external
check_pgcluster_replicas $NAMESPACE $PGO_NAMESPACE fastplatform
force_knative_services_update $NAMESPACE --selector "'fastplatform.eu/module in (core,es-an),!deploy.fastplatform.eu/db-requirement'"
kubectl apply -f deployed-manifest.yaml --selector 'fastplatform.eu/module in (core,es-cl),deploy.fastplatform.eu/db-requirement=started'
force_knative_services_update $NAMESPACE --selector "'fastplatform.eu/module in (core,es-cl),deploy.fastplatform.eu/db-requirement=started'"
if [ ! -z "${CI_ENVIRONMENT_INITIALIZATION_SCRIPT}" ]; then
  echo -n "[+] initializing core service ... "
  init_services $NAMESPACE "core"
  echo OK
fi
echo -n "[+] running Django collectstatic on web/backend service ... "
knative_service_exec $namespace "web-backend" python ../../*.binary collectstatic
echo OK
kubectl apply -f deployed-manifest.yaml --selector 'fastplatform.eu/module in (core,es-cl),deploy.fastplatform.eu/db-requirement=initialized'
force_knative_services_update $NAMESPACE --selector "'fastplatform.eu/module in (core,es-cl),deploy.fastplatform.eu/db-requirement=initialized'"

# Generate kubeconfig with a scope limited to the current namesapce
# only if executed as a GitLab pipeline
if [ ! -z "${CI_JOB_TOKEN}" ]; then
  echo "OUT__CI_ENVIRONMENT_DOMAIN=${CI_ENVIRONMENT_DOMAIN}" > out.env
  if [[ $(basename ${CI_ENVIRONMENT_DIR_SETTINGS:-""}) == "ephemeral-review" ]]; then
    generate_review_kubeconfig $NAMESPACE
  fi
fi
