#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

source $(dirname $0)/../hack/ensure-kubectl.sh
source $(dirname $0)/common.sh

# Delete the deployed resources
kubectl delete ns $NAMESPACE
