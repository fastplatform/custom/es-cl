load("@rules_pkg//:pkg.bzl", "pkg_tar")

def release(name, kustomize_resources, image_manifest, graphql_queries, visibility=None):
    """Instantiate rules to manage the release of Kustomize/Kubernetes manifests.

       Instantiated rules: 
        - :name_kustomize_manifest to build a Kustomize manifest archive
        - :name_deploy_manifest to build a ready-to-deploy Kubernetes manifest
       Kubernetes namespace can be defined via the following Bazel build argument:
        - --define namespace=target-namespace)
    """
    pkg_tar(
        name = name + "_kustomize_manifest",
        extension = "tar.gz",
        strip_prefix = "/",
        srcs = [
            ":kustomization.yaml",
            ":kustomization.addons.fertilicalc.yaml",
            ":kustomization.addons.siar-aemet.yaml",
            ":kustomization.core.yaml",
            ":kustomization.es-cl.resources.yaml",
            kustomize_resources,
        ],
        remap_paths = {
            "init": "es-cl/init",
            "manifests": "es-cl/manifests",
            "services": "es-cl/services",
            "kustomization.addons.fertilicalc.yaml": "es-cl/manifests/resources/modules/addons/fertilicalc/kustomization.yaml",
            "kustomization.addons.siar-aemet.yaml": "es-cl/manifests/resources/modules/addons/siar-aemet/kustomization.yaml",
            "kustomization.core.yaml": "es-cl/manifests/resources/modules/core/kustomization.yaml",
            "kustomization.es-cl.resources.yaml": "es-cl/manifests/resources/kustomization.yaml",
        },
        deps = [
            "@fastplatform_addons_fertilicalc_artifacts//:release_kustomize_manifest.tar.gz",
            "@fastplatform_addons_siar_aemet_artifacts//:release_kustomize_manifest.tar.gz",
            "@fastplatform_core_artifacts//:release_kustomize_manifest.tar.gz",
        ],
        visibility = visibility,
    )

    native.genrule(
        name = name + "_deploy_manifest",
        srcs = [
            ":kustomization.yaml",
            kustomize_resources,
        ],
        outs = [
            name + "-deploy-manifest.yaml",
        ],
        cmd = " && ".join([
            "KUSTOMIZE_RESOURCES_SRCS=$$(echo $(locations %s))" % kustomize_resources,
            "echo $$KUSTOMIZE_RESOURCES_SRCS | xargs -n 1 sh -c 'mkdir -p $(@D)/out/es-cl/$$(dirname $${0#$(@D)/})'",
            "echo $$KUSTOMIZE_RESOURCES_SRCS | xargs -n 1 sh -c 'cp -L $$0 $(@D)/out/es-cl/$${0#$(@D)/}'", # this to remove symlink
            "cp $(location :kustomization.yaml) $(@D)/out",
            "export HOME=~/", # workaround to source the ~/.gitconfig file of the host platform
            "$(location //tools:kustomize) build $(@D)/out > $@",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = visibility
    )

    native.genrule(
        name = name + "_kustomize_manifest_config",
        srcs = [
            image_manifest,
        ],
        outs = [
            "kustomization.yaml",
        ],
        cmd = " && ".join([ 
            "$(location //tools:kustomize) create",
            "echo resources: >> kustomization.yaml",
            "echo - es-cl/manifests >> kustomization.yaml",
            "cat $< | xargs -n 2 sh -c '$(location //tools:kustomize) edit set image $${0#*/}=$$0:$$1'",
            "mv kustomization.yaml $@",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = [
            "//visibility:private",
        ],
    )

    native.genrule(
        name = name + "_kustomize_manifest_es_cl_resources_config",
        srcs = [
            kustomize_resources,
        ],
        outs = [
            "kustomization.es-cl.resources.yaml",
        ],
        cmd = " && ".join([
            "mkdir -p $(@D)/tools $(@D)/out/es-cl/manifests/resources/modules",
            "cp manifests/resources/kustomization.yaml $(@D)/out/es-cl/manifests/resources",
            "cp $(location //tools:kustomize) $(@D)/tools/kustomize",
            "cd $(@D)/out/es-cl/manifests/resources",
            "../../../../tools/kustomize edit add resource modules",
            "cd -",
            "cp $(@D)/out/es-cl/manifests/resources/kustomization.yaml $@",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = visibility
    )

    native.genrule(
        name = name + "_kustomize_manifest_core_config",
        srcs = [
            kustomize_resources,
        ],
        outs = [
            "kustomization.core.yaml",
        ],
        cmd = " && ".join([
            "mkdir -p $(@D)/tools $(@D)/out/core $(@D)/out/es-cl/manifests/resources/modules/core",
            "cp manifests/resources/modules/core/kustomization.yaml $(@D)/out/es-cl/manifests/resources/modules/core",
            "cp $(location //tools:kustomize) $(@D)/tools/kustomize",
            "cd $(@D)/out/es-cl/manifests/resources/modules/core",
            "../../../../../../tools/kustomize edit add resource ../../../../../core",
            "cd -",
            "export HOME=~/", # workaround to source the ~/.gitconfig file of the host platform
            "cp $(@D)/out/es-cl/manifests/resources/modules/core/kustomization.yaml $@",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = visibility
    )

    native.genrule(
        name = name + "_kustomize_manifest_addons_fertilicalc_config",
        srcs = [
            kustomize_resources,
        ],
        outs = [
            "kustomization.addons.fertilicalc.yaml",
        ],
        cmd = " && ".join([
            "mkdir -p $(@D)/tools $(@D)/out/addons/fertilicalc $(@D)/out/es-cl/manifests/resources/modules/addons/fertilicalc",
            "cp manifests/resources/modules/addons/fertilicalc/kustomization.yaml $(@D)/out/es-cl/manifests/resources/modules/addons/fertilicalc",
            "cp $(location //tools:kustomize) $(@D)/tools/kustomize",
            "cd $(@D)/out/es-cl/manifests/resources/modules/addons/fertilicalc",
            "../../../../../../../tools/kustomize edit add resource ../../../../../../addons/fertilicalc",
            "cd -",
            "export HOME=~/", # workaround to source the ~/.gitconfig file of the host platform
            "cp $(@D)/out/es-cl/manifests/resources/modules/addons/fertilicalc/kustomization.yaml $@",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = visibility
    )

    native.genrule(
        name = name + "_kustomize_manifest_addons_siar_aemet_config",
        srcs = [
            kustomize_resources,
        ],
        outs = [
            "kustomization.addons.siar-aemet.yaml",
        ],
        cmd = " && ".join([
            "mkdir -p $(@D)/tools $(@D)/out/addons/siar-aemet $(@D)/out/es-cl/manifests/resources/modules/addons/siar-aemet",
            "cp manifests/resources/modules/addons/siar-aemet/kustomization.yaml $(@D)/out/es-cl/manifests/resources/modules/addons/siar-aemet",
            "cp $(location //tools:kustomize) $(@D)/tools/kustomize",
            "cd $(@D)/out/es-cl/manifests/resources/modules/addons/siar-aemet",
            "../../../../../../../tools/kustomize edit add resource ../../../../../../addons/siar-aemet",
            "cd -",
            "export HOME=~/", # workaround to source the ~/.gitconfig file of the host platform
            "cp $(@D)/out/es-cl/manifests/resources/modules/addons/siar-aemet/kustomization.yaml $@",
        ]),
        tools = [
            "//tools:kustomize",
        ],
        visibility = visibility
    )

    native.genrule(
        name = name + "_graphql_query_collections",
        srcs = [
            graphql_queries,
            "@fastplatform_core_artifacts//:query_collections.yaml",
            "@fastplatform_addons_fertilicalc_artifacts//:query_collections.yaml",
            "@fastplatform_addons_siar_aemet_artifacts//:query_collections.yaml",
            "@fastplatform_farmer_mobile_app_artifacts//:query_collections.yaml",
        ],
        outs = [
            "query_collections.yaml",
        ],
        cmd = "|".join([
            "$(location //tools:yq) e -n '[{\"name\":\"allowed-queries\", \"definition\": {\"queries\": []}}]' > $@",
            "echo $(locations %s)" % graphql_queries,
            "xargs -n 1 sh -c 'echo $$(cat $$0 | sed 's/#.*//' | tr -s \\[:space:\\] \" \")'",
            "sed -E 's/^(mutation|query|subscription)/\\1/g'",
            "(grep -E '^(mutation|query|subscription)' || true)",
            "sed 's/\"/\\\\\\\\\"/g'",
            "xargs -d '\n' -I % printf '{\"name\":\"\",\"query\":\"%\"}\n'",
            "xargs -d '\n' -I % sh -c \"q='%' $(location //tools:yq) e -i '.[0].definition.queries += env(q)' $@\";",
        ]) + ";".join([
            "nb_queries=$$($(location //tools:yq) e '.[0].definition.queries | length' $@)",
            "if (( $$nb_queries > 0 )); then for i in $$(seq 0 $$($(location //tools:yq) e '.[0].definition.queries | length' $@)); do $(location //tools:yq) e -i \".[0].definition.queries.[$$((i-1))].name = \\\"es-cl-$$(printf '%03d' $$i)\\\"\" $@; done; fi",
            "$(location //tools:yq) ea -i 'select(fileIndex == 0) *+ select(fileIndex == 1) *+ select(fileIndex == 2) *+ select(fileIndex == 3) *+ select(fileIndex == 4) | [{\"core\": .[1].definition.queries, \"es-cl\": .[0].definition.queries, \"fertilicalc\": .[2].definition.queries, \"siar-aemet\": .[3].definition.queries, \"farmer-mobile-app\": .[4].definition.queries} | {\"name\": \"allowed-queries\", \"definition\": {\"queries\": .core + .es-cl + .fertilicalc + .siar-aemet + .farmer-mobile-app}}]' $(location :query_collections.yaml) $(location @fastplatform_core_artifacts//:query_collections.yaml) $(location @fastplatform_addons_fertilicalc_artifacts//:query_collections.yaml) $(location @fastplatform_addons_siar_aemet_artifacts//:query_collections.yaml) $(location @fastplatform_farmer_mobile_app_artifacts//:query_collections.yaml)",
            "$(location //tools:yq) e -i '... style=\"\" | .[0].definition.queries[].query style=\"literal\"' $@",
        ]),
        tools = [
            "//tools:yq",
        ],
        visibility = [
            "//manifests:__pkg__",
        ],
    )

    native.genrule(
        name = name + "_graphql_allow_list",
        srcs = [
            ":query_collections.yaml",
        ],
        outs = [
            "allow_list.yaml",
        ],
        cmd = ";".join([
            "$(location //tools:yq) e '.[].name | [{\"collection\": .}]' $< > $@",
        ]),
        tools = [
            "//tools:yq",
        ],
        visibility = [
            "//manifests:__pkg__",
        ],
    )