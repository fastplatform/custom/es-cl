#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

ES_CL_CI_VERSION=${ES_CL_CI_VERSION:=$(grep 'ES_CL_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
    VERSION=$(git describe --tags)
  else
    VERSION="${ES_CL_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_ES_CL_VERSION ${VERSION}"

ES_CL_TAG=${VERSION/+/-}
if [ "$(git describe --tags --exact-match ${git_rev} >/dev/null 2>&1; echo $?)" -eq 0 ]; then
  ES_CL_TAG=${ES_CL_TAG}-release-${GITSHA}
  ES_CL_TAG=${ES_CL_TAG/+/-}
fi
echo "STABLE_ES_CL_TAG ${ES_CL_TAG}"
echo "STABLE_ES_CL_TAG_PREFIXED_WITH_COLON :${ES_CL_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="pwcfasteu.azurecr.io"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/custom/es-cl/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"
