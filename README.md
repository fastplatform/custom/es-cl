# FaST Platform / es-cl (Castilla y Leon)

The module ```es-cl``` is a module that inherits from the "[core](https://gitlab.com/fastplatform/core)" and implements customized services for Castilla y Leon. It is also based on the reuse of some [additional](https://gitlab.com/fastplatform/addons) services.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

## Services

### IACS

- [iacs/jcyl](services/iacs/jcyl)

### IDP

- [idp/authentication](services/idp/authentication)