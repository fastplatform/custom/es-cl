#!/usr/bin/env python
import os
import django
import subprocess
import sys

from django.core.management import call_command
from django.core.management.base import CommandParser

from opentelemetry import propagators, trace
from opentelemetry.exporter.jaeger import JaegerSpanExporter
from opentelemetry.instrumentation.django import DjangoInstrumentor
from opentelemetry.instrumentation.psycopg2 import Psycopg2Instrumentor
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchExportSpanProcessor
from opentelemetry.sdk.trace.propagation.b3_format import B3Format
from opentelemetry.sdk.trace.sampling import ParentBased, TraceIdRatioBased

from pathlib import Path, PurePath


HOST = os.environ.get('HOST', "0.0.0.0")
PORT = os.environ.get('PORT', 8000)

def runserver(args):
    # Run Django fake-initial migrations in a subprocess to check database connexion
    manage_py = PurePath(__file__).parent / 'manage.py'
    subprocess.run(
        ['python', manage_py, 'migrate', '--fake-initial'],
        check=True,
    )

    # Tracing instrumentation
    os.environ.setdefault("OTEL_PYTHON_DJANGO_INSTRUMENT", "True")

    jaeger_exporter = JaegerSpanExporter(
        service_name=os.environ.get(
            'OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME', 'idp-authentication'),
        agent_host_name=os.environ.get(
            'OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME', '127.0.0.1'),
        agent_port=int(os.environ.get(
            'OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT', 5775)),
    )

    sampler = TraceIdRatioBased(
        float(os.environ.get('OPENTELEMETRY_SAMPLING_RATIO', 0.1)))

    trace.set_tracer_provider(TracerProvider(sampler=ParentBased(sampler)))
    trace.get_tracer_provider().add_span_processor(
        BatchExportSpanProcessor(jaeger_exporter))
    propagators.set_global_textmap(B3Format())

    DjangoInstrumentor().instrument()
    Psycopg2Instrumentor().instrument()

    call_command('runserver', "{}:{}".format(HOST, PORT), "--noreload")


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fastplatform.settings")
    django.setup()

    parser = CommandParser()
    subcommand_parser = parser.add_subparsers(
        dest='subcommand',
        required=True,
        title='subcommands',
    )

    runserver_parser = subcommand_parser.add_parser(
        'runserver',
        help = 'run Django server (default)'
    )
    runserver_parser.set_defaults(func=runserver)

    args = parser.parse_args(sys.argv[1:] if len(sys.argv[1:]) > 0 else ['runserver'])
    args.func(args)

if __name__ == '__main__':
    main()
