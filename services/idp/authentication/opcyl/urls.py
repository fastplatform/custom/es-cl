from django.contrib.auth import views as auth_views
from django.urls import include, path
from . import views
from os import environ

urlpatterns = [
    path('', views.OpCylAuthenticationView.as_view(), name='index'),
    path('callback', views.OpCylAuthenticationCallbackView.as_view(), name='callback'),
    path('refresh', views.OpCylRefreshView.as_view(), name='refresh')
]