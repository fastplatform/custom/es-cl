from authentication.utils import import_from_settings
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model, login
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import View

import jwt
import random
import requests
import string

class OpCylAuthenticationView(View):
    """Init the Op Cyl authentication flow to get a JWT token"""

    http_method_names = ['get']

    def __init__(self, *args, **kwargs):
        self.OP_CYL_URL = import_from_settings('OP_CYL_URL')
        self.OP_CYL_API_KEY = import_from_settings('OP_CYL_API_KEY')

    def get(self, request):

        # Generate a state, which is a random string and store in the session
        state = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(16))
        request.session['opcyl_state'] = state

        redirect_url = self.OP_CYL_URL + '?key=' + self.OP_CYL_API_KEY + '&state=' + state

        return redirect(redirect_url)


class OpCylAuthenticationCallbackView(View):
    """ Once the token is send back,
     - check it through the op cyl api
     - if correct, create the user if needed and log in
     - redirect to the authentication_callback view 
    """

    http_method_names = ['get']

    def __init__(self, *args, **kwargs):
        self.UserModel = get_user_model()
        self.AUTHENTICATION_REDIRECT_URL = import_from_settings('AUTHENTICATION_REDIRECT_URL')

    def get(self, request):

        # error_message = "An error occurred while running authentication process. Please try to login again."
        error_message = "Se produjo un error al ejecutar el proceso de autenticación. Intente iniciar sesión nuevamente."

        # Catch if the user has cancelled the authentication process when being on Op Cyl
        codError = request.GET.get('codError')
        if 'codeError' is not None and codError == '401':
            messages.error(request, error_message)
            return redirect(self.AUTHENTICATION_REDIRECT_URL + '?federated_provider=opcyl')

        # Get the token  and state send back by Op Cyl
        token = request.GET.get('token')
        if token is None:
            messages.error(request, error_message)
            return redirect(self.AUTHENTICATION_REDIRECT_URL + '?federated_provider=opcyl')
    
        state = request.GET.get('state')
        # Check if the state is corresponding to the last generated
        if state == request.session.get('opcyl_state'):

            # Check the token using the Op Cyl's api
            check_url = settings.OP_CYL_CHECK_TOKEN_URL
            headers = {'Authorization': 'Bearer ' + token}
            r = requests.get(check_url, headers=headers)

            # If token is validated,
            # Create the user if needed and log in 
            if r.status_code == requests.codes.ok:
                decode = jwt.decode(token, verify=False)
                if decode['app'] == settings.OP_CYL_APP_NAME:
                    
                    # Get or create User if needed
                    user = self.get_or_create_user(decode)

                    # check if user is active
                    if user.is_active:
                        # Store token to be reused in the future
                        if user.additional_data is None:
                            user.additional_data = {'authentication': {'opcyl': {'token': token}}}
                            user.save()
                        else:
                            user.additional_data['authentication'] = {'opcyl': {'token': token}}
                            user.save()
                        if 'opcyl_action' in request.session.keys() and request.session.get('opcyl_action') == 'refresh'  and 'opcyl_refresh_redirect_url' in request.session.keys():
                            redirect_url = request.session['opcyl_refresh_redirect_url']
                            del request.session['opcyl_action']
                            del request.session['opcyl_refresh_redirect_url']
                            response = HttpResponse("", status=302)
                            response["Location"] = redirect_url
                            return response
                        else:
                            login(request, user)
                    else:
                        messages.error(request, error_message)
                        return redirect(self.AUTHENTICATION_REDIRECT_URL + '?federated_provider=opcyl')
                else:
                    messages.error(request, error_message)
                    return redirect(self.AUTHENTICATION_REDIRECT_URL + '?federated_provider=opcyl')
            else:
                messages.error(request, error_message)
                return redirect(self.AUTHENTICATION_REDIRECT_URL + '?federated_provider=opcyl')

            return redirect(self.AUTHENTICATION_REDIRECT_URL)
        
        else:
            messages.error(request, error_message)
            return redirect(self.AUTHENTICATION_REDIRECT_URL + '?federated_provider=opcyl')



    def get_or_create_user(self, decode):
        """Returns a User instance if a user is found. Creates a user if not found."""

        username = decode['usuario']
        users = self.UserModel.objects.filter(username__iexact=username)

        if len(users) == 1:
            # User has been found, we update its information with the latest received
            return self.update_user(users[0], decode)
        else:
            # User has not been found, so we create one
            user = self.create_user(decode)
            return user


    def create_user(self, decode):
        """Creates a user"""

        first_name = decode['nombre']
        last_name = decode['apellidos']
        username = decode['usuario']
        name = first_name + ' ' + last_name
        
        return self.UserModel.objects.create_user(username=username, name=name, first_name=first_name, last_name=last_name)

    def update_user(self, user, decode):
        """Update user's information with the decoded token"""
        first_name = decode['nombre']
        last_name = decode['apellidos']
        name = first_name + ' ' + last_name

        user.first_name = first_name
        user.last_name = last_name
        user.name = name
        user.save()
        return user

class OpCylRefreshView(View):
    """Init the Op Cyl authentication flow to get a JWT token"""

    http_method_names = ['get']

    def get(self, request):
        if 'redirectUrl' in request.GET:
            request.session['opcyl_refresh_redirect_url'] = request.GET['redirectUrl']
        request.session['opcyl_action'] = 'refresh'
        return redirect('/opcyl')