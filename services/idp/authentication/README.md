# idp/authentication

The idp authentication service of the module ```es-cl``` is built using the [Django](https://www.djangoproject.com/) framework.
It handles authentication as an intermediary between the [web backend](https://gitlab.com/fastplatform/core/-/tree/master/services/web/backend) and OpCyl, the authentication system for Castilla y Leon.

## Development setup

### Prerequisites

- Python 3.8+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)
- Web backend needs to be started

### Environment variables

The idp authentication expects the following environment variables to be set:

- `DJANGO_SECRET_KEY`: a secret key for Django cryptographic features (the same as web authentication)
- `POSTGRES_PASSWORD`: password to the main PostGIS database
- `AUTHENTICATION_REDIRECT_URL`: url to redirect user once login process is done
- `OP_CYL_API_KEY`: api key needed by OpCyl
- `OP_CYL_APP_NAME`: App name used in OpCyl token
- `OP_CYL_CHECK_TOKEN_URL`: url to check OpCyl token
- `OP_CYL_URL`: OpCyl url

### Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

Start the web server:
```
make start
```

The web server is now started and run at http://localhost:8001/.