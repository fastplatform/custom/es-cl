from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField


def user_default_additional_data():
    return {"placeholder": True}

class User(AbstractUser):
    id = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("identifier")
    )

    username = models.CharField(
        primary_key=True,
        error_messages={"unique": _("A user with that username already exists.")},
        help_text=_(
            "Required. 255 characters or fewer. "
            "Letters, digits and @/./+/-/_ only. "
            "For users authenticated through a third party service, this username "
            "is determined by the the third party service. The username MUST be "
            "unique accross the FaST namespace."
        ),
        max_length=255,
        verbose_name=_("username")
    )

    first_name = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("first name"),
        help_text=_("The first name(s) of the user"),
    )

    last_name = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("last name"),
        help_text=_("The last name(s) of the user"),
    )

    name = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("full name"),
        help_text=_(
            "The full name of the user. This field is not computed based on first "
            "name and last name, and must be filled manually."
        ),
    )

    additional_data = JSONField(
        default=user_default_additional_data,
        null=True,
        blank=True,
        verbose_name=_("additional data"),
        help_text=_(
            "Additional data stored for the user, as a key-value JSON, usually to "
            "store region-specific authentication data."
        ),
    )

    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name=_("email"),
        help_text=_(
            "Email address of the user, if provided by the third-party "
            "authentication system or by the user"
        ),
    )

    class Meta:
        ordering = ("username",)
        db_table = "user"
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        managed = False
    
    def save(self, *args, **kwargs):
        self.id = self.username
        super().save(*args, **kwargs)