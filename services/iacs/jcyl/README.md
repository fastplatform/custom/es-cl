# IACS/JCyL: Connector for Junta de Castilla y Leon IACS APIs

This service is a connector to access IACS data on the JCyL IACS APIs and serve these capabilities as GraphQL mutations

It exposes 4 mutations:

- SyncHoldings: request the holdings that are accessible to the user (in the JCyL IACS) and create/update the corresponding Holdings and UserRelatedParties in the FaST database
- SyncHolding: query the lines of the CAP submission of a given holding, and convert those to HoldingCampaign, Plots and Site objects in the FaST database. The service will also create the missing PlantSpecies and PlantVarieties if needed.
- InsertDemoHolding: create a demo holding for the user (where the user is the only UserRelatedParty), based on a fixed description of site/geometry available in the `demo/sites.json` file of this repository.
- DuplicateHoldingCampaign: will copy the farm holding definition (site, plots, plant varieties) from one campaign to another

The service expects the requesting user to have a current/valid OPCyL token in the FaST database (in the additional_data field of the user), as this is the authentication the service will use to query the JCyL APIs. The mutations will return errors `IACS_UNAUTHORIZED` if the OPCyL token is invalid.

### Environment variables

The environment variables used by the service are listed in the [](app/settings.py) configuration file. Default development values for some of the variables can be found in the following files:

In `service.env`:
- `API_GATEWAY_FASTPLATFORM_URL`: URL of the FaST `fastplatform` API gateway
- `API_GATEWAY_EXTERNAL_URL`: URL of the FaST `external` API gateway
- `JCYL_CONSULTA_ASESORADOS_URL`: URL to get the list of farms ("asesorados") a user is entitled to access. Defaults to: `https://ayg-des.jcyl.es/pacRS/rest/services/consultaAsesorados`
- `JCYL_CONSULTA_PRODUCTO_URL`: URL to get the list of plant species andd varieties. Defaults to `https://ayg-des.jcyl.es/pacRS/rest/services/consultaProducto`
- `JCYL_CONSULTA_PARCELA_LINEA_DECL_ASESOR_URL`: URL to get the list of the CAP subsmission lines of the user. Defaults to `https://ayg-des.jcyl.es/pacRS/rest/services/consultaParcelaLineaDeclAsesor`

In `secret.env` (encrypted):
- `API_GATEWAY_SERVICE_KEY`: secret key of the FaST API gateways service account

## Mapping JCyL IACS to FaST data model

```plantuml

package "OPCyL IACS APIs" as apis <<Rectangle>> {
    class getAsesoradosResponse {
        c_solicitante
        c_rol
    }
    class getParcelaLineaDeclaracionResponse {
        c_foi_id
        Geometria
        c_producto
        c_variedad
        c_num_orden
        c_prov_cata
        c_muni_cata
    }

    note left of getParcelaLineaDeclaracionResponse: The lines of are grouped\nby c_foi_id and the geometries\nare dissolved into a single\nmulti-polygon per group.
}

package "Context" as context <<Rectangle>> {
    class CurrentUser {
        username
    }
}

package "FaST Data Model" as fast_data_model <<Rectangle>> {
    class Holding {
        id
        authority_id
        name
    }
    class Campaign
    class UserRelatedParty {
        user : User
        role
    }
    class HoldingCampaign
    class Site {
        authority_id
        name
    }
    class Plot {
        authority_id
        name
        geometry
    }
    class Address {
        administrative_unit_level_1
        administrative_unit_level_2
        administrative_unit_level_3
        administrative_unit_level_4
    }
    class PlotPlantVariety {
        irrigation_method
        percentage
        plant_variety : PlantVariety
    }
    

    Holding "1" *-- "0..*" HoldingCampaign
    Campaign "1" *-- "0..*" HoldingCampaign
    HoldingCampaign "1" *-- "0..*" Site
    Site "1" *-- "0..*" Plot
    Plot "1" *-- "1" Address
    Plot "1" *-- "0..*" PlotPlantVariety
    Holding "1" *-- "0..*" UserRelatedParty
}

getAsesoradosResponse::c_solicitante -[#blue]-> UserRelatedParty::user
getAsesoradosResponse::c_rol -[#blue]-> UserRelatedParty::role
getParcelaLineaDeclaracionResponse::c_foi_id -[#blue]-> Plot::authority_id
getParcelaLineaDeclaracionResponse::c_num_orden -[#blue]-> Plot::name
getParcelaLineaDeclaracionResponse::Geometria -[#blue]-> Plot::geometry
getParcelaLineaDeclaracionResponse::c_prov_cata -[#blue]-> Address::administrative_unit_level_3
getParcelaLineaDeclaracionResponse::c_muni_cata -[#blue]-> Address::administrative_unit_level_2
getParcelaLineaDeclaracionResponse::c_producto -[#blue]-> PlotPlantVariety::plant_variety
getParcelaLineaDeclaracionResponse::c_variedad -[#blue]-> PlotPlantVariety::plant_variety
CurrentUser::username -[#blue]-> Holding::id
CurrentUser::username -[#blue]-> Holding::authority_id
CurrentUser::username -[#blue]-> Site::authority_id
```

## Setting up for Development

Create a virtualenv, activate it and install dependencies:
```bash
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Start the GraphQL server on port 7777:
```bash
make start
```
The server is then ready to receive queries at http://localhost:7777/graphql, with live reload enabled.

### Bazel

Bazel targets can be used to run the service locally:

```bash
bazel run .
```
or
```bash
bazel run //services/meteorology/weather
```
