import logging

from gql import gql, Client, AIOHTTPTransport
from gql.client import AsyncClientSession

from app.settings import config
from app.tracing import Tracing

logger = logging.getLogger(__name__)


class GraphQLClient:

    name: str
    url: str
    headers: dict
    client: Client
    session: AsyncClientSession

    def __init__(self, name: str, url: str, headers: dict):
        self.name = name
        self.headers = headers
        self.url = url

        transport = AIOHTTPTransport(
            url=url,
            headers=headers,
        )
        self.client = Client(
            transport=transport,
            fetch_schema_from_transport=True,
            execute_timeout=config.API_GATEWAY_TIMEOUT,
        )

    async def connect(self):
        await self.client.transport.connect()
        self.session = AsyncClientSession(client=self.client)
        logger.debug(f"GraphQLClient {self.name} connected at {self.url}")

    async def execute(self, *args, **kwargs):
        # Inject OpenTelemetry headers
        if not "extra_args" in kwargs:
            kwargs["extra_args"] = {}
        if not "headers" in kwargs["extra_args"]:
            kwargs["extra_args"]["headers"] = {}
        Tracing.inject_context(kwargs["extra_args"]["headers"])

        return await self.session.execute(*args, **kwargs)

    async def close(self):
        await self.client.transport.close()


fastplatform = GraphQLClient(
    name="fastplatform",
    url=config.API_GATEWAY_FASTPLATFORM_URL,
    headers={
        "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
        "Content-type": "application/json",
    },
)

external = GraphQLClient(
    name="external",
    url=config.API_GATEWAY_EXTERNAL_URL,
    headers={
        "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
        "Content-type": "application/json",
    },
)