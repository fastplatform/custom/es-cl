import os
import json
import logging

import requests

import httpx
from shapely.geometry import shape

from app.settings import config
from app.api.lib.gis import Projection, add_crs


logger = logging.getLogger(__name__)


class JCyLClient:
    def __init__(self):
        self.http_client = None

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    async def consulta_asesorados(self, opcyl_token):
        """Request the list of NIFs that the user is allowed to see in the IACS db

        Args:
            opcyl_token (str): the token returned by the OPCyL auth when the user logged in
        """
        response = await self.http_client.get(
            config.JCYL_CONSULTA_ASESORADOS_URL,
            headers={
                "Authorization": f"Bearer {opcyl_token}",
                "Accept": "application/json",
            },
            timeout=config.JCYL_TIMEOUT,
        )

        response.raise_for_status()
        data = response.json()

        return data

    async def consulta_parcela_linea_decl_asesor(self, opcyl_token, holding_id):
        """Request the list of parcels (lines of the CAP application)

        Args:
            holding_id (str): the holding being requested (corresponds to the NIF of the
                              titular, in FaST)
            opcyl_token(str): the token returned by the OPCyL auth when the user logged in
        """
        response = await self.http_client.get(
            config.JCYL_CONSULTA_PARCELA_LINEA_DECL_ASESOR_URL,
            params={"c_asesorado": holding_id},
            headers={
                "Authorization": f"Bearer {opcyl_token}",
                "Accept": "application/json",
            },
            timeout=config.JCYL_TIMEOUT,
        )

        response.raise_for_status()
        data = response.json()

        return data

    async def consulta_producto(
        self, opcyl_token, plant_species_id=None, plant_variety_id=None
    ):
        """Request details of a plant species/variety

        Args:
            plant_species_id (str)
            plant_variety_id(str)
        """
        params = {}
        if plant_species_id is not None:
            params.update({"c_producto": plant_species_id})
        if plant_variety_id is not None:
            params.update({"c_variedad": plant_variety_id})

        response = await self.http_client.get(
            config.JCYL_CONSULTA_PRODUCTO_URL,
            params=params,
            headers={
                "Authorization": f"Bearer {opcyl_token}",
                "Accept": "application/json",
            },
            timeout=config.JCYL_TIMEOUT,
        )

        response.raise_for_status()
        data = response.json()

        return data


jcyl_client = JCyLClient()