import logging
import json
from pprint import pprint
import itertools

import httpx
import graphene
from gql import gql
from graphql import GraphQLError

from opentelemetry import trace

from shapely import wkt, ops
import pyproj
from shapely.geometry.multipolygon import MultiPolygon

from app.api.lib.jcyl import jcyl_client

from app.settings import config

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class SyncHolding(graphene.Mutation):
    class Arguments:
        holding_id = graphene.String(required=True)
        campaign_id = graphene.Int(required=True)
        iacs_year = graphene.Int(required=False)

    plot_ids = graphene.List(graphene.String)

    async def mutate(self, info, holding_id, campaign_id, iacs_year=None):

        request = info.context["request"]
        client = request.state.graphql_clients["fastplatform"]

        # Retrieve info about the holding we are supposed to gather
        # geometries for
        with tracer().start_as_current_span("query_holding_info"):
            logger.debug("query_holding_info")

            query = (config.API_DIR / "graphql/query_holding_by_id.graphql").read_text()
            variables = {"holding_id": holding_id}

            # Impersonate the user
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": request.headers["X-Hasura-User-Id"],
                    "X-Hasura-Role": request.headers["X-Hasura-Role"],
                }
            }

            response = await client.execute(
                gql(query), variables, extra_args=extra_args
            )

            # Holding does not exist or the requesting user is not a
            # related party to the holding and hence is not allowed to make any
            # edit to the holding
            if not response["holding"]:
                raise GraphQLError("HOLDING_DOES_NOT_EXIST")

            holding_name = response["holding"][0]["name"]

        # Get the OPCyL token of the user (when he/she logged into FaST)
        with tracer().start_as_current_span("query_opcyl_token"):
            logger.debug("query_opcyl_token")

            query = (
                config.API_DIR / "graphql/query_user_additional_data.graphql"
            ).read_text()

            response = await client.execute(
                gql(query),
                extra_args=extra_args,
            )

            try:
                additional_data = response["private_user_info"][0]["additional_data"]
                opcyl_token = additional_data["authentication"]["opcyl"]["token"]
            except:
                # If there is no OPCyL token, just fail
                raise GraphQLError("IACS_UNAUTHORIZED")

        with tracer().start_as_current_span("query_parcela_linea_decl_asesor"):
            logger.debug("query_parcela_linea_decl_asesor")

            try:
                data = await jcyl_client.consulta_parcela_linea_decl_asesor(
                    opcyl_token,
                    holding_id,
                )
            except httpx.HTTPStatusError as e:
                if e.response.status_code in [
                    httpx.StatusCode.UNAUTHORIZED,
                    httpx.StatusCode.FORBIDDEN,
                ]:
                    # The token has likely expired
                    raise GraphQLError("IACS_UNAUTHORIZED")
                raise GraphQLError("IACS_REMOTE_ERROR")

            if not data:
                raise GraphQLError("IACS_REMOTE_ERROR")

            try:
                # We have requested only one holding (via the c_asesorado parameter)
                # so we will get at most one result
                data = data[0]
                # Get the rows of the submission
                lines = data["LineasDeclaracion"]
            except:
                logger.exception(f"Received invalid data from JCyL {json.dumps(data)}")
                raise GraphQLError("IACS_REMOTE_ERROR")

            if not lines:
                # The application exists but has no lines
                return SyncHolding(plot_ids=[])

            # Sort before grouping
            lines = sorted(lines, key=lambda l: l["c_foi_id"])

            # Remove the lines that have no geometry
            lines = list(filter(lambda l: "Geometria" in l, lines))

            # Group and dissolve geometries
            features_of_interest = []

            groups = itertools.groupby(lines, lambda l: l["c_foi_id"])

            for i, (foi_id, line_group) in enumerate(groups):
                if i > config.JCYL_MAX_FOIS_PER_FARM:
                    logger.error(
                        f"Response from JCYL was truncated at {config.JCYL_MAX_FOIS_PER_FARM} records {len(lines)} lines)."
                    )                
                    break

                line_group = list(line_group)

                # Some FOI ids can be bad (seen on the dev endpoints), like the foi_id="0"
                # -> just discard the foi_id
                if foi_id in config.JCYL_FOI_IDS_TO_TRASH:
                    logger.warning(
                        f"FOI {foi_id} with {len(lines)} lines has been voluntarily discarded as per JCYL_FOI_IDS_TO_TRASH={config.JCYL_FOI_IDS_TO_TRASH}."
                    )
                    continue

                # The plot geometry is the union of the polygons composing the FOI
                geometry = ops.unary_union(
                    [
                        wkt.loads(p.get("Geometria", p.get("geometria")))
                        for p in line_group
                    ]
                )

                # Convert the polygon to a MultiPolygon (geometry type for Plot in the database)
                if geometry.geom_type == "Polygon":
                    geometry = MultiPolygon([geometry])

                # Convert to a __geo_interface__ dict and assign CRS
                geometry = geometry.__geo_interface__
                geometry["crs"] = {"type": "name", "properties": {"name": "EPSG:4258"}}

                # The FOI is supposed to be provided as parcels of the *same crop* by definition
                # so we can just use the first parcel of the group
                plant_species_id = line_group[0].get("c_producto", None)
                plant_variety_id = line_group[0].get("c_variedad", None)
                if plant_species_id is not None and plant_variety_id is not None:
                    plant_variety_id = f"{plant_species_id}.{plant_variety_id}"
                else:
                    plant_variety_id = None

                try:
                    administrative_unit_level_3_id = (
                        f'ES-07-{int(line_group[0]["c_prov_cata"]):02d}'
                    )
                except:
                    logger.exception('Bad value for administrative_unit_level_3_id')
                    administrative_unit_level_3_id = None

                try:
                    administrative_unit_level_4_id = f'ES-07-{int(line_group[0]["c_prov_cata"]):02d}-{int(line_group[0]["c_muni_cata"]):03d}'
                except:
                    logger.exception('Bad value for administrative_unit_level_4_id')
                    administrative_unit_level_4_id = None

                features_of_interest += [
                    {
                        "authority_id": foi_id,
                        "name": ", ".join([p["c_num_orden"] for p in line_group]),
                        "geometry": geometry,
                        "plant_variety": {
                            "id": plant_variety_id,
                            "name": "",
                        },
                        "address": {
                            "administrative_unit_level_1_id": "ES",  # Spain
                            "administrative_unit_level_2_id": "ES-07",  # Castilla y Leon
                            "administrative_unit_level_3_id": administrative_unit_level_3_id,  # provincia from response
                            "administrative_unit_level_4_id": administrative_unit_level_4_id,  # municipio from response
                        },
                    }
                ]

            # Find the plant varieties and products that do not alreay exist in our
            # database
            needed_plant_variety_ids = list(
                set(
                    [
                        foi["plant_variety"]["id"]
                        for foi in features_of_interest
                        if foi["plant_variety"]["id"] is not None
                    ]
                )
            )

            query = (
                config.API_DIR / "graphql/query_plant_variety_in_list.graphql"
            ).read_text()
            variables = {"plant_variety_ids": needed_plant_variety_ids}

            response = await client.execute(gql(query), variables)

            existing_plant_varieties = {
                plant_variety["id"]: plant_variety
                for plant_variety in response["plant_variety"]
            }

            for foi in features_of_interest:
                if foi["plant_variety"]["id"] not in existing_plant_varieties:
                    logger.error(
                        f"Plant variety {foi['plant_variety']['id']} returned by JCyL does not exist in FaST and will be discarded."
                    )
                    foi["plant_variety_ids"] = []
                else:
                    foi["plant_variety_ids"] = [foi["plant_variety"]["id"]]

        with tracer().start_as_current_span("build_upsert_holding_campaigns"):
            logger.debug("build_upsert_holding_campaigns")

            # Build the GraphQL mutation data
            holding_campaigns = [
                {
                    "campaign_id": campaign_id,
                    "holding_id": holding_id,
                    "sites": {
                        "data": [
                            {
                                "authority_id": holding_id,
                                "name": holding_name,
                                "plots": {
                                    "data": [
                                        {
                                            "authority_id": foi["authority_id"],
                                            "name": foi["name"],
                                            "geometry": foi["geometry"],
                                            "address": {
                                                "data": foi["address"],
                                                "on_conflict": {
                                                    "constraint": "address_pkey",
                                                    "update_columns": [
                                                        "administrative_unit_level_1_id",
                                                        "administrative_unit_level_2_id",
                                                        "administrative_unit_level_3_id",
                                                        "administrative_unit_level_4_id",
                                                    ],
                                                },
                                            },
                                            "plot_plant_varieties": {
                                                "data": [
                                                    {"plant_variety_id": pvi}
                                                    for pvi in foi["plant_variety_ids"]
                                                ],
                                                "on_conflict": {
                                                    "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                                    "update_columns": [
                                                        "plot_id",
                                                        "plant_variety_id",
                                                    ],
                                                },
                                            },
                                        }
                                        for foi in features_of_interest
                                    ],
                                    "on_conflict": {
                                        "constraint": "plot_authority_id_site_id_unique",
                                        "update_columns": [
                                            "authority_id",
                                            "site_id",
                                            "name",
                                            "geometry",
                                        ],
                                    },
                                },
                            }
                        ],
                        "on_conflict": {
                            "constraint": "site_authority_id_holding_campaign_id_unique",
                            "update_columns": [
                                "authority_id",
                                "holding_campaign_id",
                                "name",
                            ],
                        },
                    },
                }
            ]

        # Write out the objects, for debug
        # pprint(objects, indent=2)
        # with open("objects.json", "w") as f:
        #    f.write(json.dumps(holding_campaigns, indent=2))

        with tracer().start_as_current_span("insert_holding_campaign"):
            logger.debug("insert_holding_campaign")
            mutation = (
                config.API_DIR / "graphql/mutation_insert_holding_campaigns.graphql"
            ).read_text()
            variables = {"holding_campaigns": holding_campaigns}

            response = await client.execute(
                gql(mutation),
                variables,
            )

        # with tracer().start_as_current_span("build_upsert_plot_plot_groups"):
        #     logger.debug("build_upsert_plot_plot_groups")

        #     # We need the insert the plot-plot_group relationships in a second query
        #     # (we could not do it in the first one)
        #     plot_groups_as_inserted = {
        #         plot_group["authority_id"]: plot_group["id"]
        #         for plot_group in response["insert_holding_campaign"]["returning"][0][
        #             "plot_groups"
        #         ]
        #     }

        #     plots_as_inserted = response["insert_holding_campaign"]["returning"][0][
        #         "sites"
        #     ][0]["plots"]

        #     plot_plot_groups = [
        #         {
        #             "plotgroup_id": plot_groups_as_inserted[
        #                 site["plots"][plot["authority_id"]]["plot_group"][
        #                     "authority_id"
        #                 ]
        #             ],
        #             "plot_id": plot["id"],
        #         }
        #         for plot in plots_as_inserted
        #     ]

        # with tracer().start_as_current_span("insert_plot_plot_groups"):
        #     logger.debug("insert_plot_plot_groups")

        #     mutation = (
        #         config.API_DIR / "graphql/mutation_insert_plot_plot_groups.graphql"
        #     ).read_text()
        #     variables = {"plot_plot_groups": plot_plot_groups}

        #     response = await client.execute(gql(mutation), variables)

        return SyncHolding(
            plot_ids=sorted([foi["authority_id"] for foi in features_of_interest]),
        )
