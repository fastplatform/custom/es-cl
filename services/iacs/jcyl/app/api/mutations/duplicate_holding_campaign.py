import logging
import json

import requests
import graphene
from gql import gql
from graphql import GraphQLError
from opentelemetry import trace

from app.settings import config

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class DuplicateHoldingCampaign(graphene.Mutation):
    """Duplicate a holding campaign (down to PlotPlantVartiety only)"""

    class Arguments:
        holding_id = graphene.String()
        campaign_from_id = graphene.Int()
        campaign_to_id = graphene.Int()

    holding_campaign_id = graphene.Int()

    async def mutate(self, info, holding_id, campaign_from_id, campaign_to_id):

        request = info.context["request"]
        client = request.state.graphql_clients["fastplatform"]

        # Retrieve the holding campaign that we are supposed to duplicate
        with tracer().start_as_current_span("query_holding_campaign"):
            logger.debug("query_holding_campaign")

            query = (
                config.API_DIR / "graphql/query_holding_campaign.graphql"
            ).read_text()
            variables = {"holding_id": holding_id, "campaign_id": campaign_from_id}

            # Impersonate the user
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": request.headers["X-Hasura-User-Id"],
                    "X-Hasura-Role": request.headers["X-Hasura-Role"],
                }
            }

            response = await client.execute(
                gql(query), variables, extra_args=extra_args
            )

            # Holding campaign does not exist or the requesting user is not a
            # related party to the holding and hence is not allowed to read it
            if not response["holding_campaign"]:
                raise GraphQLError("HOLDING_CAMPAIGN_DOES_NOT_EXIST")

            holding_campaign_from = response["holding_campaign"][0]

        with tracer().start_as_current_span("build_upsert_holding_campaigns"):
            logger.debug("build_upsert_holding_campaigns")

            # Build the GraphQL mutation data
            holding_campaigns = [
                {
                    "campaign_id": campaign_to_id,
                    "holding_id": holding_id,
                    "sites": {
                        "data": [
                            {
                                "authority_id": site["authority_id"],
                                "name": site["name"],
                                "plots": {
                                    "data": [
                                        {
                                            "authority_id": plot["authority_id"],
                                            "name": plot["name"],
                                            "geometry": plot["geometry"],
                                            "plot_plant_varieties": {
                                                "data": [
                                                    plot_plant_variety
                                                    for plot_plant_variety in plot[
                                                        "plot_plant_varieties"
                                                    ]
                                                ],
                                                "on_conflict": {
                                                    "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                                    "update_columns": [
                                                        "plot_id",
                                                        "plant_variety_id",
                                                    ],
                                                },
                                            },
                                        }
                                        for plot in site["plots"]
                                    ],
                                    "on_conflict": {
                                        "constraint": "plot_authority_id_site_id_unique",
                                        "update_columns": [
                                            "authority_id",
                                            "site_id",
                                            "name",
                                            "geometry",
                                        ],
                                    },
                                },
                            }
                            for site in holding_campaign_from["sites"]
                        ],
                        "on_conflict": {
                            "constraint": "site_authority_id_holding_campaign_id_unique",
                            "update_columns": [
                                "authority_id",
                                "holding_campaign_id",
                                "name",
                            ],
                        },
                    },
                }
            ]

        with tracer().start_as_current_span("insert_holding_campaign"):
            logger.debug("insert_holding_campaign")

            mutation = (
                config.API_DIR / "graphql/mutation_insert_holding_campaigns.graphql"
            ).read_text()
            variables = {"holding_campaigns": holding_campaigns}

            response = await client.execute(
                gql(mutation),
                variables,
            )

        return DuplicateHoldingCampaign(
            holding_campaign_id=response["insert_holding_campaign"]["returning"][0][
                "id"
            ],
        )
