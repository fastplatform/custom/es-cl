import logging
import json

import graphene
from gql import gql
from graphql import GraphQLError

from opentelemetry import trace
import httpx

from app.settings import config
from app.api.lib.jcyl import jcyl_client

# Log
logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


ROLE_MAPPING = {"ASESORADO": "advisor", "PROPIA": "farmer"}


class SyncHoldings(graphene.Mutation):
    """Mutation for synchronizing the holdings of a user

    Expects the 'X-Hasura-User-Id' header to be set by the authentication hook

    Returns:
        SyncHoldings: A GraphQL node containing ok, errors and the holding_ids of the
                      farms that have been created/updated for the user
    """

    class Arguments:
        pass

    holding_ids = graphene.List(graphene.String)

    async def mutate(self, info):

        request = info.context["request"]
        client = request.state.graphql_clients["fastplatform"]

        # This will intentionally fail if Hasura has not injected the X-Hasura-User-Id header
        user_id = request.headers["X-Hasura-User-Id"]

        # Get the OPCyL token of the user (when he/she logged into FaST)
        with tracer().start_as_current_span("query_opcyl_token"):
            logger.debug("query_opcyl_token")

            query = (
                config.API_DIR / "graphql/query_user_additional_data.graphql"
            ).read_text()

            # Force the user role to "farmer"
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": request.headers["X-Hasura-User-Id"],
                    "X-Hasura-Role": request.headers["X-Hasura-Role"],
                }
            }

            response = await client.execute(
                gql(query),
                extra_args=extra_args,
            )

            try:
                additional_data = response["private_user_info"][0]["additional_data"]
                opcyl_token = additional_data["authentication"]["opcyl"]["token"]
            except:
                # If there is no OPCyL token, just fail
                raise GraphQLError("IACS_UNAUTHORIZED")

        # Retrieve the list of farms the user is allowed to see
        with tracer().start_as_current_span("query_consulta_asesorados"):
            logger.debug("query_consulta_asesorados")

            try:
                data = await jcyl_client.consulta_asesorados(opcyl_token)
            except httpx.HTTPStatusError as e:
                if e.response.status_code in [
                    httpx.StatusCode.UNAUTHORIZED,
                    httpx.StatusCode.FORBIDDEN,
                ]:
                    # The token has likely expired
                    raise GraphQLError("IACS_UNAUTHORIZED")
                raise GraphQLError("IACS_REMOTE_ERROR")

        with tracer().start_as_current_span("build_holdings_upsert"):
            logger.debug("build_holdings_upsert")

            # Convert items tp upsert statements
            holdings = [
                {
                    "id": item["c_solicitante"],
                    "name": item["c_solicitante"],
                    "user_related_parties": {
                        "data": [
                            {
                                "role": ROLE_MAPPING.get(item["c_rol"], "farmer"),
                                "user_id": user_id,
                            }
                        ],
                        "on_conflict": {
                            "constraint": "user_related_party_holding_id_user_id_unique",
                            "update_columns": ["role"],
                        },
                    },
                }
                for item in data
            ]

        # Insert the holdings and set this user as a related party
        # Note that this will fail (due to foreign key constraint violation)
        # if the user does not exist
        with tracer().start_as_current_span("insert_holdings"):
            logger.debug("insert_holdings")

            mutation = (
                config.API_DIR / "graphql/mutation_insert_holdings.graphql"
            ).read_text()
            response = await client.execute(
                gql(mutation),
                {"objects": holdings},
            )

            return SyncHoldings(holding_ids=sorted([o["id"] for o in holdings]))
