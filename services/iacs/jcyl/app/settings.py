import os
import sys

import logging

from pathlib import Path, PurePath
from typing import Set
from pydantic import BaseSettings

from typing import Dict, Union

class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_ETRS89_UTM30N: str = "25830"

    API_GATEWAY_EXTERNAL_URL: str
    API_GATEWAY_FASTPLATFORM_URL: str
    # 2 minutes, there can be some heavy farms to write to Hasura
    API_GATEWAY_TIMEOUT: int = 120
    API_GATEWAY_SERVICE_KEY: str

    OPENTELEMETRY_EXPORTER_JAEGER_SERVICE_NAME: str = "iacs-arpea"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_HOSTNAME: str = "127.0.0.1"
    OPENTELEMETRY_EXPORTER_JAEGER_AGENT_PORT: int = 5775
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1

    JCYL_CONSULTA_ASESORADOS_URL: str
    JCYL_CONSULTA_PRODUCTO_URL: str
    JCYL_CONSULTA_PARCELA_LINEA_DECL_ASESOR_URL: str
    JCYL_TIMEOUT: int
    JCYL_FOI_IDS_TO_TRASH: Set[str] = ["0"]
    JCYL_MAX_FOIS_PER_FARM = 5000

    IACS_DISPLAY_NAME: str = "Organismo Pagador de la Comunidad de Castilla y León"
    IACS_SHORT_NAME: str = "OPCyL"
    IACS_WEBSITE_URL: str = "https://agriculturaganaderia.jcyl.es/web/es/organismo-pagador-castilla-leon.html"

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

config = Settings()